
package science.unlicense.p.calculator;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.MessagePredicate;
import science.unlicense.display.api.desktop.FrameMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.system.path.Paths;

/**
 *
 * @author Johann Sorel
 */
public class CalculatorFrame {

    public static void main(String[] args) {
        new CalculatorFrame();
    }

    private final UIFrame frame;
    private final WContainer pane = new WContainer(new BorderLayout());
    private final WButtonBar bar = new WButtonBar();
    private final WTextField text = new WTextField();

    public CalculatorFrame() {
        frame = SwingFrameManager.INSTANCE.createFrame(false);
        frame.getContainer().setLayout(new BorderLayout());

        bar.setLayout(new LineLayout());

        final WMenuButton exit = new WMenuButton(new Chars("Exit"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                System.exit(0);
            }
        });

        final WMenuDropDown menu = new WMenuDropDown();
        menu.setText(new Chars("File"));
        menu.getDropdown().addChild(exit,null);

        bar.addChild(menu,null);
        bar.addChild(createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")), FillConstraint.builder().coord(1, 0).build());
        bar.addChild(createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")), FillConstraint.builder().coord(2, 0).build());
        createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")).doClick();

        final WContainer defaultActions = new WContainer(new GridLayout(0,3));
        defaultActions.addChild(createTextButton(new Chars("7"), new Chars("7")), null);
        defaultActions.addChild(createTextButton(new Chars("8"), new Chars("8")), null);
        defaultActions.addChild(createTextButton(new Chars("9"), new Chars("9")), null);
        defaultActions.addChild(createTextButton(new Chars("4"), new Chars("4")), null);
        defaultActions.addChild(createTextButton(new Chars("5"), new Chars("5")), null);
        defaultActions.addChild(createTextButton(new Chars("6"), new Chars("6")), null);
        defaultActions.addChild(createTextButton(new Chars("1"), new Chars("1")), null);
        defaultActions.addChild(createTextButton(new Chars("2"), new Chars("2")), null);
        defaultActions.addChild(createTextButton(new Chars("3"), new Chars("3")), null);
        defaultActions.addChild(createTextButton(new Chars("0"), new Chars("0")), null);
        defaultActions.addChild(createTextButton(new Chars("."), new Chars(".")), null);


        final WContainer paneAction = new WContainer(new BorderLayout());
        paneAction.addChild(bar, BorderConstraint.TOP);
        paneAction.addChild(defaultActions, BorderConstraint.CENTER);

        final WContainer paneLog = new WContainer(new BorderLayout());
        paneLog.addChild(text, BorderConstraint.BOTTOM);


        frame.getContainer().addChild(paneAction, BorderConstraint.LEFT);
        frame.getContainer().addChild(paneLog, BorderConstraint.CENTER);


        frame.setTitle(new Chars("Unlicense - Calculator"));
        frame.setSize(1280, 800);
        frame.addEventListener(new MessagePredicate(FrameMessage.class), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                FrameMessage message = (FrameMessage) event.getMessage();
                if (message.getType() == FrameMessage.TYPE_PREDISPOSE) {
                    System.exit(0);
                }
            }
        });
        frame.setVisible(true);

    }


    private static WMenuButton createThemeButton(final Chars path, Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                    WStyle style = RSReader.readStyle(Paths.resolve(path));
                    for(int i=0;i<style.getRules().getSize();i++){
                        final StyleDocument r = (StyleDocument) style.getRules().get(i);
                        WidgetStyles.mergeDoc(
                            SystemStyle.INSTANCE.getRule(r.getName()),
                            r, false);
                    }

                    SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }

    private WButton createTextButton(Chars title, final Chars value) {
        final WButton button = new WButton(title, null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                CharArray t = text.getText().concat(value);
                text.setText(t);
            }
        });

        return button;
    }
}
